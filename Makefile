SRC := CancerNeutral_2.tex
BIB := clean.bib
DROPBOX := ~/Dropbox/Shared/Innan/mbe-neutral-cancer
TEX := latexmk

PRODUCT := $(SRC:.tex=.wmi.pdf)
PDF := $(SRC:.tex=.pdf)
BBL := $(SRC:.tex=.bbl)

.DEFAULT_GOAL := all
.PHONY: all bbl clean open run bib

export:
	rsync -auvn \
${PRODUCT} \
${SRC} \
clean.bib \
mbe.bst \
MBET.sty \
Figs \
${DROPBOX}/

import:
	rsync -auv --include="*.tex" --exclude="*" ${DROPBOX}/ ./

all: ${PDF}
	open -a Skim $<

bbl:
	$(MAKE) clean
	$(MAKE) all

${PDF}: ${SRC}
	${TEX} $<
	cp -a ${PDF} ${PRODUCT}

${BIB}: bibdesk-export.bib
	./trim_bib.py $< | ./rename_bib.py - -o $@

bib:
	$(MAKE) ${BIB}

clean:
	$(RM) ${PRODUCT} ${PDF} ${BBL} *.dvi *.xdv *.toc *.out *.aux *.log *.blg

open:
	open ${DROPBOX}

run:
	open ${PDF}
