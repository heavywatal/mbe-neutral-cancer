\documentclass[10pt,twocolumn,twoside]{article}

\usepackage{amsthm,amsmath} % must be called ahead of mathspec
\usepackage[all,warning]{onlyamsmath}

\usepackage{graphics}
\usepackage{color}
\usepackage{colortbl}
\usepackage{MBET}
\usepackage{newtxtext,newtxmath}

\usepackage{natbib}
\bibpunct{(}{)}{;}{a}{}{,}

\usepackage{graphicx}

\renewcommand{\baselinestretch}{1.1}

\addtolength{\oddsidemargin}{-.2cm}
\addtolength{\evensidemargin}{-1.2cm}

\addtolength{\textwidth}{1.5cm}
\addtolength{\topmargin}{-1.75cm}
\addtolength{\textheight}{3.5cm}

\renewcommand{\textfraction}{0.01}
\renewcommand{\topfraction}{0.99}
\renewcommand{\bottomfraction}{0.65}
\renewcommand{\floatpagefraction}{0.90}
\renewcommand{\dbltopfraction}{0.95}
\renewcommand{\dblfloatpagefraction}{0.80}
\renewcommand{\sfdefault}{phv}

\makeatletter
\renewcommand{\footnotesep}{-2pt}
\makeatletter

% space of double hline in Table
\doublerulesep=0.4pt

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE]{\thepage\hspace{2em}Innan \emph{et al.}}
\fancyhead[RO]{\thepage}
\fancyhead[C]{}

% erase the line in footnote
\renewcommand{\headrulewidth}{0pt}

\fancypagestyle{plain}{
    \fancyhf{}
}

\title{Neutral Theory for a Cancer Cell Population}


\author{
  Hideki Innan$^{1,*}$,
  Atsushi Niida$^{2}$,
  Watal M. Iwasaki$^{1}$  \vspace{2mm}\\
  \small $^1$ SOKENDAI, The Graduate University for Advanced Studies, Hayama, Kanagawa 240--0193, Japan\\[0.1cm]
  \small $^2$ \textcolor{red}{SOKENDAI, The Graduate University for Advanced Studies, Hayama, Kanagawa 240--0193, Japan}\\[0.1cm]
  \small $^*$ Corresponding author: E-mail: innan\_hideki@soken.ac.jp
  %\thanks{E\-mail: innan\_hideki@soken.ac.jp.}
}

\date{\small Manuscript intended for \emph{Molecular Biology and Evolution, \today}}

%\abstract{abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstractabstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract. abstract}

\begin{document}
\maketitle



In addition to the proposal of the ``neutral mutation hypothesis''~\cite[]{Kimura68neutral, Kimura68genetres,KimuraOhta71nature}, Kimura's major contribution to population genetics is the development of fundamental theoretical framework of the behavior of the frequency of neutral (and also non-neutral) alleles mainly in a Wright-Fisher population~\cite[]{Kimura55a, Kimura55b, Kimura64,Kimura68genetres}.
\textcolor{green}{
Neutral theory provides beautiful mathematical expressions on the amounts and patterns of polymorphism and divergence, which are still very useful in the modern population genetics at the molecular level, in which we are mainly interested in polymorphism of DNA sequences.
}
\textcolor{blue}{
Neutral theory provides beautiful mathematical expressions on the amounts and patterns of polymorphism and divergence.
They are still very useful in the modern population genetics at the molecular level, in which we are mainly interested in polymorphism of DNA sequences.
}
The neutral theory is particularly simple for handling a single nucleotide polymorphism (SNP)~\cite[]{Kimura69}.
As the mutation rate per site is very low, a bi-allelic single-locus model with no recurrent mutation can be reasonably applied, where the probability density distribution of the frequency of the derived allele is approximately given by $\phi(x)\propto 1/x$ in a Wright-Fisher population.
This is directly expanded to the infinite-site model that assumes that any new mutation creates a novel polymorphic site: The expected number of sites with derived allele frequency $i/N$ is given by
\begin{equation}\label{eq:const}
S(i)=N\mu \frac{1}{i}
\end{equation}
in an asexual population with $N$ haploids where $\mu$ is the mutation rate per region~\cite[]{Kimura69}.

In molecular population genetics, the neutral theory provides quantitative predictions that can be applied to actual observed data.
A genome-wide deviation of allele frequency spectrum (AFS) from equation~(\ref{eq:const}) implies that the population size has not been constant over time, and theoretical predictions of neutral alleles in various population models provide an opportunity to infer demographic models that explain the data well.
A commonly used alternative model is an exponentially growing population, in which excess of rare alleles is expected in comparison with a constant-size population (fig.~\ref{fig:growth-afs}).
Based on this theoretical prediction, it is possible to estimate the growth rate, $r$, by likelihood-based statistical methods~\cite[e.g.,][]{wooding2002matrix}.
In addition to the past demography, natural selection is another important factor to affect AFS, and the action of natural selection could be documented by testing neutral predictions.
Thus, the neutral theory provides the whole theoretical basis of molecular population genetics for living organisms.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE
\begin{figure*}[tb]
  \begin{center}
   \vspace{-2mm}
   \includegraphics[width=\textwidth]{Figs/Fig1}
   \caption{\small (A) Exponentially growing populations with different growth rates. (B) Expected allele frequency spectra in exponentially growing populations. The colors are identical to those in (A).
   }\label{fig:growth-afs}
\vspace{-6mm}
  \end{center}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE


We here describes how Kimura's neutral theory works in a cancer cell population, in which there is an increasing interest in genetic variation within a tumor, or ITH (Intra-Tumor Heterogeneity).
In the following, we first overview how a cancer cell population forms and evolves, and then explain a couple of fundamental differences between cancer cell populations and asexual organismal populations.



\section*{Evolution of Cancer Cell Population}
In 1976, Nowell proposed that cancer originates from a single normal cell, and cell clones acquiring somatic mutations are subject to natural selection in a stepwise manner~\cite[]{nowell1976clonal}.
In other words, cancer evolution can be regarded as Darwinian evolution of a unicellular organism that undergo asexual reproduction.
Since this clonal evolution model was proposed, a number of proto-oncogenes and tumor suppressor genes have been discovered.
In 1990,~\cite{FearonVogelstein1990} integrated these discoveries with the clonal evolution model, thereby proposing the multistep tumorigenesis model; in colorectal tumorigenesis, while accumulating strong driver mutations in APC, KRAS, TP53 etc., a normal epithelial cell linearly transforms through a benign tumor to a malignant tumor (fig.~\ref{fig:possible-models}A).
Since then, the view that linear Darwinian evolution shapes a uniform population of malignant cells has been well accepted.


However, genomic studies employing next-generation sequencers have recently demonstrated that cancer cell populations may not be genetically uniform, rather there could be many types of subclones, exhibiting extensive ITH\@.
\textcolor{blue}{
A fundamental question is whether ITH consists of subclones with different physiological properties (e.g., growth rate; fig.~\ref{fig:possible-models}B) or just neutral mutations (fig.~\ref{fig:possible-models}C).
}
This is a serious problem because if the former is the case, we have to be extra careful to monitor cancer progression, that is, non-neutral ITH should affect the probability of recurrent and metastatic cancer.
Thus, full understanding of the evolutionary process to generate ITH will be essential for early detection and treatment decisions~\cite[]{vogelstein2013cancer, yates2012nrg, burrell2013n}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE
\begin{figure*}[tb]
  \begin{center}
   \vspace{-2mm}
   \includegraphics[width=1.9\columnwidth]{Figs/Fig2}
   \caption{\small Illustration of possible models for the evolution of cancer cell population and ITH\@.
   (A) Linear Darwinian model, in which driver mutations (red stars) repeatedly fix through evolution. A low level of ITH is expected.
   (B) Non-neutral model for ITH, where extensive ITH arises through driver mutations (red stars).
   (C) Neutral model for ITH, where extensive ITH arises through neutral mutations (open boxes?). \textcolor{red}{AN: please redraw B and C with the size of stars constant with A. Maybe different configurations for B and C.}  }\label{fig:possible-models}
\vspace{-6mm}
  \end{center}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE


Cancer genomics usually relies on next-generation sequencing of a piece from a tumor tissue (usually consists of \textcolor{red}{$10^{6\sim}$ AN: OK?}cells) compared with the sequence of a normal tissue as a control.
Deep sequencing is a very powerful means for studying ITH because it allows us to evaluate the level of genetic variation within a sampled tissue.
Deep sequencing produces sequence reads at a much higher coverage than usually (typically from hundreds to thousands), from which we could estimate the frequencies of somatic mutations in the cancer cell population.
While some somatic mutations that occurred in the early phases of cancer progression are nearly fixed, some new mutations could be ``polymorphic'' in the cell population, which constitute ITH\@.
Applying deep sequencing to multiple regions in a single tumor is even more powerful to understand the entire picture of ITH, and in the recent few years, this multiregion sequencing has been applied to many types of solid cancer~\cite[]{mcgranahan2017clonal}.
It is found that the observed patterns are quite heterogeneous depending on cancer type.

\textcolor{red}{AN: Where in this para should we cite~\cite{PMID:22608083}~\cite{PMID:22397650}? }

In \textcolor{red}{``slowly developing'' types of cancer represented by renal cancer~\cite[]{PMID:22397650,PMID:24487277} and low-grade glioma~\cite[]{PMID:25848751} AN: OK?}, a number of subclonal (i.e., polymorphic) mutations were found in well known cancer driver genes, indicating loss of function of these genes could cause fitness differences between subclones  (fig.~\ref{fig:possible-models}B).
More interestingly, multiple subclonal mutations existed in the same gene or genes that work together in the same molecular pathway, consistent with the pattern of convergent evolution.
These observations would fit to the scenario of non-neutral ITH (fig.~\ref{fig:possible-models}B).

In contrast, recent genomic analyses of multiregion sequencing data together with mathematical modeling have established a novel view: for some types of cancer such as colorectal cancer, the majority of ITH may be shaped by ``neutral evolution'' (fig.~\ref{fig:possible-models}C).
For example, extensive ITH was unveiled by multiregion sequencing of colorectal cancer, but very few subclonal mutations were identified in known driver genes~\cite[]{uchi2016integrated}.
\citet{sottoriva2015big} also found extensive ITH in colorectal tumors that uniformly spread over the entire tumors.
Moreover, the authors found that mutations identified in a region of tumor were often shared with geographically separated regions.
Based on this observation, the authors proposed the Big-Bang model, which states that a large number of subclones are generated in the early phases of cancer evolution, after which they expand without selective competition, while partially mixing, to eventually shape uniformly high ITH in the entire region of a tumor.
\citet{sottoriva2015big} and~\citet{uchi2016integrated} independently demonstrated that certain simulation models could produce similar patterns to  their observations (see~\citet{IwasakiInnan2017tumopp} for detailed arguments on the conditions under which this holds).

A new approach for distinguishing the neutral and non-neutral scenarios of ITH is to incorporate information of allele frequency distribution~\cite[]{williams2016identification,bozic2016quantifying,sun2017between}.
\citet{williams2016identification} showed that, under neutrality, the expected number of mutations has a linear correlation with the inverse of their frequencies.
Therefore, deviation from this prediction should be considered as a signature of selection.
This approach is powerful because it can be applied to deep sequencing data from a single region of a tumor, thereby allowing pan-cancer analyses.
\citet{williams2016identification} applied this method to multiple cancer types and found that the neutral prediction well holds for many cases, although the power of this analysis to detect selection is under debate~\cite[]{noorbakhsh2017ng}.
\citet{bozic2016quantifying} provided a more mathematical treatment and also showed that their neutral prediction was consistent with observed data.
While these analyses use deep sequencing data from a single region of sample for each tumor, \citet{sun2017between} proposed a method to compare multiple regions.

In summary, ITH has been receiving extensive attention particularly in recent few years, but statistical analyses are quite simple and rather intuitive without taking full advantage of the highly sophisticated statistical methodologies developed for organismal populations (see refs in this issue).
To solve this problem, it is crucial to understand the difference between cancer cell populations and organismal populations, which is described in the next section.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE
\begin{figure*}[tb]
  \begin{center}
   \vspace{-2mm}
   \includegraphics[width=1.5\columnwidth]{Figs/Cancertree}
   \caption{\small (A) An exponentially growing population (green), within which a string advantageous mutation occurred (the red star) and this subclone has spread more rapidly (pink).
   (B,C) Suppose the scenario of (A) is applied to an asexual species, where the generation time is constant over the population.
   (B) AFS for derived SNPs within the original and derived subclones and (C) a gene tree reconstructed from the observed pattern of SNPs.
   (D,E) Suppose the scenario of (A) is applied to a cancer cell population, where the longevity of cells is very long (i.e., $b\gg d$).
   AFS and a gene tree are shown in (B) and (C).
   }\label{fig:trees}
\vspace{-6mm}
  \end{center}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE

\section*{Neutral Theory for a Cancer cell population}
As a cancer cell population evolves through self proliferation from a single origin cell, one might think that population genetic theory for an asexual Wright-Fisher population may be applicable.
However, this is not strictly true because there is no concept of ``generation'' in a cancer cell population~\cite[]{sidow2015concepts}.
Whereas all individuals die and replaced by new offspring every generation in a Wright-Fisher population, cancer cells proliferate very quickly but very hardly die because they likely avoid apoptosis, and
as a consequence, the population size in general increases dramatically.
The situation of a cancer cell population can be better described by the branching process that is specified by the birth and death rates ($b$ and $d$, respectively), where the population growth rate is given by $r=b-d$~\cite[e.g., ][]{durrett2015branching}.
Through the following arguments, we assume $b\gg d$, which should be reasonable in a rapidly growing cancer cell population.
Then, under neutrality (i.e., the birth and death rates are uniform for all cells), $S(i)$ is given by
\begin{equation}\label{eq:S_expo}
		S(i) = N \mu \sum_{k=0}^{\infty} \frac{1}{(i+k)(i+k+1)} {\left(\frac{d}{b}\right)}^{k}
\end{equation}~\cite[]{OhtsukiInnan2017tpb}.
We thus can obtain the neutral prediction of allele frequency spectrum for a cancer cell population, which could provide powerful statistical basis for estimating past demography and testing neutrality (or detecting selection) as we commonly do with SNP data from organismal populations.

However, there are a couple of caveats in doing so.
First is the use of the shape of AFS for inferring the past demographic model (assuming neutrality).
This should work also in a cancer cell population, but the shape of spectrum may not be very informative because
equation~(\ref{eq:S_expo}) converges to
\begin{equation}\label{eq:S_expo2}
S(i)\rightarrow N\mu \frac{1}{i(i-1)},
\end{equation}
as $r$ increases (or $\phi(x)\rightarrow 1/x^2$), which is independent of the growth rate.
This is why similar AFS are predicted for large $r$ in figure~\ref{fig:growth-afs}B, thereby indicating that it is very difficult to obtain a reliable estimate of $r$ from the shape of AFS for a very rapidly growing population.

Second is on inferring the role of selection.
As a common sense in analyzing SNP data from organismal populations, the absolute number of SNPs is also informative to infer the past events especially selection~\cite{nielsen2013introduction} (cite more form this issue).
This may be true also in a cancer cell population, but we need more careful consideration.
To explain this, let us consider a simple exponentially growing population (shown in green) as illustrated in fig.~\ref{fig:trees}A (we do not assume neutrality here).
Quite recently, an advantageous variant arose within this population (shown in pink), and it has grown more rapidly than the original type.
Then, at present, it is assumed that the sizes of the original and derived types are the same, say $N$, from which seven individuals are sampled.
The true genealogical relationship is shown by the solid line in figure~\ref{fig:trees}A.
In such a situation in an organismal population, we expect a reduced amount of variation within the derived type in comparison with the original types as illustrated in figure~\ref{fig:trees}B, which shows the absolute allele frequent distributions of SNPs.
In contrast, in the same situation applied to a cancer cell population, from equation~(\ref{eq:S_expo}), we expect that the allele frequency distributions for the original and derived types are very similar in both the absolute amount and the shape assuming $r$ for both types are very large (see fig.~\ref{fig:trees}C).
\textcolor{blue}{
This crucial difference results from a simple assumption that most mutations arise through cell division; the speed of cell division is accelerated in the advantageous subclones in the cancer cell population, but not in the asexual organismal population.
}
In other words, the ``molecular clock'' works along regular time (or generation) in the asexual organismal population, whereas the clock proceeds as the number of cell division increases, and the speed of cell division may differ between lineages.

\section*{Perspectives}
There is no doubt that growing medical interests are in ITH in cancer cell populations, and the fundamental goals should overlap with those in population genetics.
Population geneticists have developed a number of powerful statistical tools for understanding evolutionary mechanisms behind polymorphism data including SNPs;
Kimura's neutral theory and other classic theory provide the whole basis of such computational tools.
Population genetics should readily contribute to cancer genetics once
the differences between cancer cell populations and organismal populations are taken into account.
\textcolor{blue}{
We already mentioned the relationship between time and reproduction.
Another example is the significance of the spatial structure in the development of solid tumor.
Growing cells compete with each other because cell proliferation depend on various resources, such as space, oxygen, and other nutrients.
Cell differentiation and hierarchical age structure may also affect the formation of ITH\@.
Cancer stem cells can maintain themselves by self-renewal, but their differentiated daughters are allowed to divide only limited times.
Those physiological constraints are so severe that contingent pattern of positional relationship among neighboring cells can have a larger effect on the outcome than the variation in the rate of growth and death do.
A closer look at those kind of ecology and development of cell populations will expand our view of what can happen under neutrality.
We believe that it is a promising way to fully understand the dynamics of cancer evolution.
}

\citep{ling2015pnas}









%\section*{Acknowledgments}


\bibliographystyle{mbe}%%%%Bibliography style file
\bibliography{clean}%,niida}
\end{document}
