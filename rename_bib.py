#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Rename bib entries
"""
import sys


tr = {
    'bozic2016pcb': 'bozic2016quantifying',
    'burrell2013n': 'burrell2013n',
    'durrett2015': 'durrett2015branching',
    'fearon1990c': 'FearonVogelstein1990',
    'gerlinger2012em': 'PMID:22397650',
    'gerlinger2014ng': 'PMID:24487277',
    'iwasaki2017po': 'IwasakiInnan2017tumopp',
    'kimura1955pnas': 'Kimura55a',
    'kimura1955cshsqb': 'Kimura55b',
    'kimura1964ap': 'Kimura64',
    'kimura1968gr': 'Kimura68genetres',
    'kimura1968n': 'Kimura68neutral',
    'kimura1969g0': 'Kimura69',
    'kimura1971n': 'KimuraOhta71nature',
    'mcgranahan2017c': 'mcgranahan2017clonal',
    'nielsen2013': 'nielsen2013introduction',
    'nik-zainal2012c': 'PMID:22608083',
    'nowell1976s': 'nowell1976clonal',
    'ohtsuki2017tpb': 'OhtsukiInnan2017tpb',
    'sidow2015tg': 'sidow2015concepts',
    'sottoriva2015ng': 'sottoriva2015big',
    'sun2017ng': 'sun2017between',
    'suzuki2015ng': 'PMID:25848751',
    'uchi2016pg': 'uchi2016integrated',
    'vogelstein2013s': 'vogelstein2013cancer',
    'williams2016ng': 'williams2016identification',
    'wooding2002g': 'wooding2002matrix',
    'yates2012nrg': 'yates2012nrg',
}


def replace_all(content):
    for patt, repl in tr.items():
        content = content.replace(patt, repl)
    return content


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--outfile',
                        type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument('bib', type=argparse.FileType('r'))
    args = parser.parse_args()
    x = replace_all(args.bib.read())
    args.outfile.write(x)


if __name__ == '__main__':
    main()
